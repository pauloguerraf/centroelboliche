#include <FastLED.h>
#define NUM_LEDS 100
#define DATA_PIN 3
CRGB leds[NUM_LEDS];
int inicios[] = {0, 20, 40, 60, 80};
int finales[] = {19, 39, 59, 79, 99};
void setup()
{
  Serial.begin(115200);
  FastLED.addLeds<WS2812B, DATA_PIN, GRB>(leds, NUM_LEDS);
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i] = CRGB(0, 0, 0);
  }
  FastLED.show();
  delay(500);
}

void loop()
{
  for (int i = 0; i < 5; i++) {
    prendergrupo(i);
    delay(1000);
    apagargrupo(i);
  }
}

void prendergrupo(int num_) {
  for (int i = inicios[num_]; i < finales[num_]; i++) {
    leds[i] = CRGB(200, 200, 200);
  }
  FastLED.show();
  delay(500);
}

void apagargrupo(int num_) {
  for (int i = inicios[num_]; i < finales[num_]; i++) {
    leds[i] = CRGB(0, 0, 0);
  }
  FastLED.show();
  delay(500);
}
