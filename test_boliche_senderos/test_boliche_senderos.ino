#include <FastLED.h>
#include "DFRobotDFPlayerMini.h"
#define NUM_LEDS 219
#define DATA_PIN 3
CRGB leds[NUM_LEDS];
int inicios[] = {24, 59, 78, 47, 127, 0, 196, 172, 151, 64};
int finales[] = {46, 63, 126, 58, 150, 23, 219, 195, 171, 77};
int botones[] = {43, 41, 39, 37, 35, 53, 49, 47, 51, 45, 33, 31, 29};
unsigned long duraciones[] = {28000, 20000, 15000, 19000, 27000, 18000, 27000,
                              14000, 11000, 20000, 25000, 24000,
                              10000, 15000, 26000, 14000, 21000,
                              12000, 11000, 18000
                             };
//unsigned long duraciones[] = {3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000,
//                          3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000
//                       };
boolean EN = false;
boolean MUTE = false;
DFRobotDFPlayerMini myDFPlayer;
void printDetail(uint8_t type, int value);
unsigned long start = 0;
unsigned long duracion = 0;
boolean playing = false;
int grupo = -1;
int lastAudio = -1;
void setup() {
  Serial3.begin(9600);
  Serial.begin(115200);
  Serial.println();
  Serial.println(F("DFRobot DFPlayer Mini Demo"));
  Serial.println(F("Initializing DFPlayer ... (May take 3~5 seconds)"));

  if (!myDFPlayer.begin(Serial3)) {  //Use softwareSerial to communicate with mp3.
    Serial.println(F("Unable to begin:"));
    Serial.println(F("1.Please recheck the connection!"));
    Serial.println(F("2.Please insert the SD card!"));
    while (true) {
      delay(0); // Code to compatible with ESP8266 watch dog.
    }
  }
  Serial.println(F("DFPlayer Mini online."));
  myDFPlayer.volume(25);  //Set volume value. From 0 to 30


  FastLED.addLeds<WS2812B, DATA_PIN, GRB>(leds, NUM_LEDS);
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i] = CRGB(0, 0, 0);
  }
  FastLED.show();
  delay(500);
}

void loop() {
  for (int i = 0; i < 13; i++) {
    int a = digitalRead(botones[i]);
    if (a == 0) {
      if (i == 12) { //mute
        MUTE = !MUTE;
        delay(500);
        if (MUTE)myDFPlayer.volume(0);
        else myDFPlayer.volume(25);
        break;
      }
      else if (i == 11) { //ingles
        EN = true;
        MUTE = false;
        if (playing) {
          myDFPlayer.pause();
          playing = false;
          apagartodo();
        }
        break;
      }
      else if (i == 10) { //espanol
        EN = false;
        MUTE = false;
        if (playing) {
          myDFPlayer.pause();
          playing = false;
          apagartodo();
        }
        break;
      }
      else {
        apagartodo();
        if (!playing || i != lastAudio) {
          if (!EN) {
            Serial.print("pin " );
            Serial.print( botones[i]);
            Serial.print ("numero " );
            Serial.print(i + 1);
            Serial.println();
            grupo = i;
            prendergrupo(grupo);
            myDFPlayer.play(i + 1); //audio mas 1
            lastAudio = i;
            delay(500);
            playing = true;
            duracion = duraciones[i];
            start = millis();
            break;
          }
          else if (EN) {
            Serial.print("pin " );
            Serial.print( botones[i]);
            Serial.print ("numero " );
            Serial.print(i + 11);
            Serial.println();
            grupo = i;
            prendergrupo(grupo);
            myDFPlayer.play(i + 11); //audio mas 1
            lastAudio = i;
            delay(500);
            playing = true;
            duracion = duraciones[i + 10];
            start = millis();
            break;
          }

        }
      }
    }
  }
  if (millis() - start > duracion && playing) {
    playing = false;
    if (grupo >= 0)apagargrupo(grupo);
  }
  if (myDFPlayer.available()) {
    printDetail(myDFPlayer.readType(), myDFPlayer.read()); //Print the detail message from DFPlayer to handle different errors and states.
  }
}

void prendergrupo(int num_) {
  for (int i = inicios[num_]; i < finales[num_]; i++) {
    leds[i] = CRGB(200, 200, 200);
  }
  FastLED.show();
  delay(500);
}

void apagargrupo(int num_) {
  for (int i = inicios[num_]; i < finales[num_]; i++) {
    leds[i] = CRGB(0, 0, 0);
  }
  FastLED.show();
  delay(500);
  grupo = -1;
}

void apagartodo() {
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i] = CRGB(0, 0, 0);
  }
  FastLED.show();
  delay(100);
}
void printDetail(uint8_t type, int value) {
  switch (type) {
    case TimeOut:
      Serial.println(F("Time Out!"));
      break;
    case WrongStack:
      Serial.println(F("Stack Wrong!"));
      break;
    case DFPlayerCardInserted:
      Serial.println(F("Card Inserted!"));
      break;
    case DFPlayerCardRemoved:
      Serial.println(F("Card Removed!"));
      break;
    case DFPlayerCardOnline:
      Serial.println(F("Card Online!"));
      break;
    case DFPlayerUSBInserted:
      Serial.println("USB Inserted!");
      break;
    case DFPlayerUSBRemoved:
      Serial.println("USB Removed!");
      break;
    case DFPlayerPlayFinished:
      Serial.print(F("Number:"));
      Serial.print(value);
      Serial.println(F(" Play Finished!"));
      break;
    case DFPlayerError:
      Serial.print(F("DFPlayerError:"));
      switch (value) {
        case Busy:
          Serial.println(F("Card not found"));
          break;
        case Sleeping:
          Serial.println(F("Sleeping"));
          break;
        case SerialWrongStack:
          Serial.println(F("Get Wrong Stack"));
          break;
        case CheckSumNotMatch:
          Serial.println(F("Check Sum Not Match"));
          break;
        case FileIndexOut:
          Serial.println(F("File Index Out of Bound"));
          break;
        case FileMismatch:
          Serial.println(F("Cannot Find File"));
          break;
        case Advertise:
          Serial.println(F("In Advertise"));
          break;
        default:
          break;
      }
      break;
    default:
      break;
  }
}
